FROM openjdk:8-jdk
VOLUME /tmp
ARG JAR_FILE
ADD ${JAR_FILE} user-management.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "user-management.jar"]