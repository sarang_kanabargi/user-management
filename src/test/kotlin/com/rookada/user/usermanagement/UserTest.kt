package com.rookada.user.usermanagement

import com.rookada.user.usermanagement.domain.User
import org.junit.Before
import org.junit.Test

class UserTest {
    private var user: User? = null

    @Before
    fun setUp() {
        user = User("", "Sarang", "Kanabargi", "PAN", "AADHAR")
    }

    @Test
    fun createUser() {
        assert(user?.firstName == "Sarang")
    }
}
