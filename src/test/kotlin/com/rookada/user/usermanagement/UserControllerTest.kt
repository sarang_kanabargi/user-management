package com.rookada.user.usermanagement

import com.rookada.user.usermanagement.controller.UserController
import com.rookada.user.usermanagement.repository.UserRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@RunWith(MockitoJUnitRunner::class)
class UserControllerTest {

    @InjectMocks
    private lateinit var userController: UserController

    @Mock
    private lateinit var userRepository: UserRepository

    private var mockMvc: MockMvc? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .build()
    }

    @Test
    fun userControllerTest() {
        mockMvc?.perform(get("/api/user"))
                ?.andExpect(status().isOk)
                ?.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    }
}
