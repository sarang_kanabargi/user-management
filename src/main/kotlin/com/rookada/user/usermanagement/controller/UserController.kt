package com.rookada.user.usermanagement.controller

import com.rookada.user.usermanagement.domain.User
import com.rookada.user.usermanagement.repository.UserRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/user")
class UserController(private val userRepository: UserRepository) {

    @GetMapping()
    fun getUser(): List<User> {
        return userRepository.findAll()
    }

}