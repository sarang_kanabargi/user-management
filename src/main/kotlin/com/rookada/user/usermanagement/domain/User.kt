package com.rookada.user.usermanagement.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document
class User(@Id val id: String?,
           val firstName: String,
           val lastName: String,
           val pan: String,
           val aadhar: String,
           val createdDtTm: LocalDateTime = LocalDateTime.now()
)