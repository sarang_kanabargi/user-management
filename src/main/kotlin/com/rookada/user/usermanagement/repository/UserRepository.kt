package com.rookada.user.usermanagement.repository

import com.rookada.user.usermanagement.domain.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository : MongoRepository<User, String>